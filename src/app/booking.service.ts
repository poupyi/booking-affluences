import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  url = 'http://localhost:8080/resource/1337/available';
  constructor(private http: HttpClient) { }

  getRoomAvailability(datetime: string) {
    const options = {
      params: { datetime }
    };

    return this.http.get(this.url, options);
  }
}
