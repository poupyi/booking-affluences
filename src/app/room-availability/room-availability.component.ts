import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BookingService } from '../booking.service';
import { ResultDialogComponent } from '../result-dialog/result-dialog.component';
import * as moment from 'moment';

@Component({
  selector: 'app-room-availability',
  templateUrl: './room-availability.component.html',
  styleUrls: ['./room-availability.component.scss']
})
export class RoomAvailabilityComponent implements OnInit {
  selectedDate: moment.Moment;
  selectedTime: moment.Moment;
  timeSlots: string[] = [];
  constructor(private bookingService: BookingService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.generateTimeSlots();
  }

  generateTimeSlots() {
    const start = moment('June 11, 2020 00:00:00');
    const format = 'HH:mm:ss';

    for (let hour = 0; hour < 24; hour++) {
      start.minutes(0);
      start.hour(hour);
      this.timeSlots.push(start.format(format));
      start.minute(30);
      this.timeSlots.push(start.format(format));
    }
  }

  onDateChanged(event) {
    this.selectedDate = moment(event.value);
  }

  onTimeChanged(event) {
    const time = event.value.split(':');
    const hour = time[0];
    const min = time[1];
    this.selectedTime = moment().hour(hour).minutes(min).seconds(0);
  }

  onCheckAvailability() {
    const finalDateTime = moment(this.selectedDate);

    finalDateTime.hour(this.selectedTime.hour());
    finalDateTime.minutes(this.selectedTime.minutes());
    finalDateTime.seconds(this.selectedTime.seconds());

    this.bookingService.getRoomAvailability(finalDateTime.format('YYYY-MM-DDTHH:mm:ss')).subscribe(result => {
      this.dialog.open(ResultDialogComponent, {
        data: {
          ...result,
          dateTime: `${finalDateTime.format('YYYY-MM-DD')} at ${finalDateTime.format('LTS')}`
        }
      });
    });
  }
}
