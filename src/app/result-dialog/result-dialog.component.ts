import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

interface ResultData {
  available: boolean,
  dateTime: string
}

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html',
  styleUrls: ['./result-dialog.component.scss']
})
export class ResultDialogComponent implements OnInit {
  availability: boolean;
  dateTime: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ResultData) { 
    this.availability = data.available;
    this.dateTime = data.dateTime;
  }

  ngOnInit(): void {
  }

}
